export const routes = [
    {
        path: "/",
        name: "下载币充值",
        component: () => import("@/layout/index.vue"),
        redirect: "/download",
        children: [
            {
                path: "/download",
                component: () => import("@/views/download/index.vue"),
            },
        ],
    }]